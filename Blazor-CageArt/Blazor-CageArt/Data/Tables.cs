﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace Blazor_CageArt.Data
{
    public class Account
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Login { get; set; }
        [Required]
        public string Password { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string UserRole { get; set; }
        public string EnycryptPassword()
        {
            var hashbytes = SHA512.Create().ComputeHash(Encoding.UTF8.GetBytes(Login + Password));
            var hashbuilder = new StringBuilder();
            foreach (var hashbyte in hashbytes)
            {
                hashbuilder.Append(hashbyte.ToString("x2"));
            }
            return hashbuilder.ToString();
        }
    }
    public class Message
    {
        public int MessageId { get; set; }
        [Required]
        [MaxLength(255)]
        public string MessageText { get; set; }
        public Thread MessageThread { get; set; }
        public Account MessageAuthor { get; set; }
        public DateTime MessageDate { get; set; }

    }

    public class Thread
    {
        public int ThreadId { get; set; }
        [Required]
        [MaxLength(100)]
        public string ThreadName { get; set; }
        [Required]
        [MaxLength(255)]
        public string ThreadDescription { get; set; }
        public DateTime CreationDate { get; set; }
        public Account ThreadAuthor { get; set; }

    }
    public class NavigationLink
    {
        public int IdLink { get; set; }
        public string Adress { get; set; }
        public string Header { get; set; }
        public string Icon { get; set; }
        public string AccessLevel { get; set; }
        
    }

    public class Character
    {
        public int CharacterId { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterName { get; set; }
        [Required]
        [MaxLength(20)]
        public string CharacterGender { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterAge { get; set; }
        [Required]
        [MaxLength(20)]
        public string CharacterZodiak { get; set; }
        [Required]
        [MaxLength(20)]
        public string CharacterBlood { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterBirthday { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterWeight { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterAbility { get; set; }
        [Required]
        [MaxLength(100)]
        public string CharacterStatus { get; set; }
        public Article CharacterArticle { get; set; }
    }
    public class Article 
    {
        public int ArticleId { get; set; }
        [Required]
        [MaxLength(65535)]
        public string ArticleText { get; set; }
    }
    public class Mask
    {
        public int MaskId { get; set; }
        [Required]
        [MaxLength(100)]
        public string MaskType { get; set; }
        [Required]
        [MaxLength(100)]
        public string MaskRarity { get; set; }

    }
    public class Skin
    {
        public int SkinId { get; set; }
        [Required]
        [MaxLength(100)]
        public string SkinName { get; set; }
        [Required]
        [MaxLength(100)]
        public string SkinAbility { get; set; }
        [MaxLength(100)]
        public string SkinDisadvantage { get; set; }

    }
    public class Location
    {
        public int LocationId { get; set; }
        [Required]
        [MaxLength(100)]
        public string LocationName { get; set; }
        [Required]
        [MaxLength(100)]
        public string LocationPosition { get; set; }
        [Required]
        [MaxLength(100)]
        public string LocationType { get; set; }
        [Required]
        [MaxLength(100)]
        public string LocationDwellers { get; set; }
        [Required]
        [MaxLength(100)]
        public string LocationLife { get; set; }
    }
    public class GalleryImage
    {
        public int ImageId { get; set; }
        public string Path { get; set; }
    }
}
