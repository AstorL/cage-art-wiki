﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Blazor_CageArt.Data
{
    public class ArticleService : MariaDBResolver<Article>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectArticles = new MySqlCommand("SELECT * FROM article");
        private MySqlCommand InsertArticles = new MySqlCommand("INSERT INTO article (article_description) VALUES(@Article)");
        public ArticleService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Article> GetData(MySqlCommand command)
        {
            var ArticleService = new ObservableCollection<Article>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var character in BufferedData)
                {
                    ArticleService.Add(new Article { ArticleId = (int)character[0], ArticleText = (string)character[1] });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении статей\n {E.Message}\n в следующем участке кода\n {E.StackTrace}");
            }
            return ArticleService;
        }
        public ObservableCollection<Article> GetArticles() => GetData(SelectArticles);
        public void InsertArticle(Article article)
        {
            InsertArticles.Parameters.AddWithValue("@Article", article.ArticleText);
            RunCommand(InsertArticles);
            InsertArticles.Parameters.Clear();
        }
    }

    public class MaskService : MariaDBResolver<Mask>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectMasks = new MySqlCommand("SELECT * FROM mask");
        private MySqlCommand InsertMasks = new MySqlCommand("INSERT INTO mask (mask_type,rarity)VALUES(@Type,@Rarity)");
        public MaskService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Mask> GetData(MySqlCommand command)
        {
            var MaskService = new ObservableCollection<Mask>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var mask in BufferedData)
                {
                    MaskService.Add(new Mask
                    { MaskId = (int)mask[0], MaskRarity = (string)mask[1], MaskType = (string)mask[2] });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении масок\n {E.Message}\n в следующем участке кода\n {E.StackTrace}");
            }
            return MaskService;
        }
        public ObservableCollection<Mask> GetMasks() => GetData(SelectMasks);
        public void InsertMask(Mask mask)
        {
            InsertMasks.Parameters.AddWithValue("@Type", mask.MaskType);
            InsertMasks.Parameters.AddWithValue("@Rarity", mask.MaskRarity);
            RunCommand(InsertMasks);
            InsertMasks.Parameters.Clear();
        }
    }

    public class SkinService : MariaDBResolver<Skin>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectSkins = new MySqlCommand("SELECT * FROM skin");
        private MySqlCommand InsertSkins = new MySqlCommand(@"INSERT INTO skin (skin_name,skin_ability,skin_dis)
	    VALUES (@Skin,@Ability,@Disadvantage)");
        public SkinService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Skin> GetData(MySqlCommand command)
        {
            var SkinService = new ObservableCollection<Skin>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var skin in BufferedData)
                {
                    SkinService.Add(new Skin
                    { SkinId = (int)skin[0], SkinName = (string)skin[1], SkinAbility = (string)skin[2] });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении скинов\n {E.Message}\n в следующем участке кода\n {E.StackTrace}");
            }
            return SkinService;
        }
        public ObservableCollection<Skin> GetSkins() => GetData(SelectSkins);
        public void InsertSkin(Skin skin)
        {
            InsertSkins.Parameters.AddWithValue("@Skin", skin.SkinName);
            InsertSkins.Parameters.AddWithValue("@Ability", skin.SkinAbility);
            InsertSkins.Parameters.AddWithValue("@Disadvantage", skin.SkinDisadvantage);
            RunCommand(InsertSkins);
            InsertSkins.Parameters.Clear();
        }
    }

    public class LocationService : MariaDBResolver<Location>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectLocations = new MySqlCommand("SELECT * FROM location");
        private MySqlCommand InsertLocations = new MySqlCommand(@"NSERT INTO location 
        (loc_name,loc_pos,loc_type,people,`work`)
	    VALUES (@Name,@Position,@Type,@Dwellers,@Life)");
        public LocationService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<Location> GetData(MySqlCommand command)
        {
            var LocationService = new ObservableCollection<Location>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var location in BufferedData)
                {
                    LocationService.Add(new Location
                    {
                        LocationId = (int)location[0],
                        LocationName = (string)location[1],
                        LocationPosition = (string)location[2],
                        LocationType = (string)location[3],
                        LocationDwellers = (string)location[4],
                        LocationLife = (string)location[5]
                    });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении локаций\n {E.Message}\n в следующем участке кода\n {E.StackTrace}");
            }
            return LocationService;
        }
        public ObservableCollection<Location> GetLocations() => GetData(SelectLocations);
        public void InsertLocation(Location location)
        {
            InsertLocations.Parameters.AddWithValue("@Name", location.LocationName);
            InsertLocations.Parameters.AddWithValue("@Position", location.LocationPosition);
            InsertLocations.Parameters.AddWithValue("@Type", location.LocationType);
            InsertLocations.Parameters.AddWithValue("@Dwellers", location.LocationDwellers);
            InsertLocations.Parameters.AddWithValue("@Life", location.LocationLife);
            RunCommand(InsertLocations);
            InsertLocations.Parameters.Clear();
        }
    }

    public class CharacterService : MariaDBResolver<Character>
    {
        private IConfiguration Configuration;
        private AccountService AccountService;
        private ArticleService ArticleService;
        private LocationService LocationService;
        private SkinService SkinService;
        private MaskService MaskService;
        private MySqlCommand SelectCharacters = new MySqlCommand("SELECT * FROM `character`");
        private MySqlCommand SelectAuthors = new MySqlCommand("SELECT * FROM char_has_users");
        private MySqlCommand SelectLocations = new MySqlCommand("SELECT * FROM char_location");
        private MySqlCommand SelectSkins = new MySqlCommand("SELECT * FROM char_skin");
        private MySqlCommand SelectMasks = new MySqlCommand("SELECT * FROM char_mask");
        private MySqlCommand InsertCharacters = new MySqlCommand(@"INSERT INTO `character` (name,gender,age,zodiak,blood,birthday,weight,ability,status,article_id) VALUES 
        (@Name,@Gender,@Age,@Zodiak,@Blood,@Birthday,@Weight,@Ability,@Status,@Article)");
        private MySqlCommand AddAuthors = new MySqlCommand("INSERT INTO char_has_users (id_character,user_id) VALUES (@Character,@User)");
        private MySqlCommand AddLocations = new MySqlCommand("INSERT INTO char_location(id_character, id_location) VALUES(@Character, @Location)");
        private MySqlCommand AddSkins = new MySqlCommand("INSERT INTO char_skin(id_character, id_skin) VALUES(@Character, @Skin)");
        private MySqlCommand AddMasks = new MySqlCommand("INSERT INTO char_mask(id_character, id_mask) VALUES(@Character, @Mask)");

        public CharacterService(IConfiguration Configuration) : base(Configuration)
        {
            this.Configuration = Configuration;
            AccountService = new AccountService(this.Configuration);
            ArticleService = new ArticleService(this.Configuration);
            LocationService = new LocationService(this.Configuration);
            SkinService = new SkinService(this.Configuration);
            MaskService = new MaskService(this.Configuration);
        }
        protected override ObservableCollection<Character> GetData(MySqlCommand command)
        {
            var CharacterService = new ObservableCollection<Character>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var character in BufferedData)
                {
                    var article = from articles in ArticleService.GetArticles() where articles.ArticleId == (int)character[10] select articles;
                    CharacterService.Add(new Character
                    {
                        CharacterId = (int)character[0],
                        CharacterName = (string)character[1],
                        CharacterGender = (string)character[2],
                        CharacterAge = (string)character[3],
                        CharacterZodiak = (string)character[4],
                        CharacterBlood = (string)character[5],
                        CharacterBirthday = (string)character[6],
                        CharacterWeight = (string)character[7],
                        CharacterAbility = (string)character[8],
                        CharacterStatus = (string)character[9],
                        CharacterArticle = article.First()
                    });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"Ошибка в получении персонажей\n {E.Message}\n в следующем участке кода\n {E.StackTrace}");
            }
            return CharacterService;
        }
        public ObservableCollection<Character> GetCharacters() => GetData(SelectCharacters);
        public void AddLocation(Location location, Character character)
        {
            var Loc = from locations in LocationService.GetLocations() where locations.LocationId == location.LocationId select location;
            AddLocations.Parameters.AddWithValue("@Character", character.CharacterId);
            if (Loc.Count() == 0)
            {
                LocationService.InsertLocation(location);
                var InsLoc = from locations in LocationService.GetLocations()
                             where locations.LocationName == location.LocationName
                             select location;
                AddLocations.Parameters.AddWithValue("@Location", InsLoc.First().LocationId);
                RunCommand(AddLocations);
                AddLocations.Parameters.Clear();
            }
            else
            {
                AddLocations.Parameters.AddWithValue("@Location", Loc.First().LocationId);
                RunCommand(AddLocations);
                AddLocations.Parameters.Clear();
            }
        }
        public void AddSkin(Skin skin, Character character)
        {
            var Skin = from skins in SkinService.GetSkins() where skins.SkinId == skin.SkinId select skin;
            AddSkins.Parameters.AddWithValue("@Character", character.CharacterId);
            if (Skin.Count() == 0)
            {
                SkinService.InsertSkin(skin);
                var InsSkin = from skins in SkinService.GetSkins()
                             where skins.SkinName == skin.SkinName
                             select skins;
                AddSkins.Parameters.AddWithValue("@Skin", InsSkin.First().SkinId);
                RunCommand(AddSkins);
                AddSkins.Parameters.Clear();
            }
            else
            {
                AddSkins.Parameters.AddWithValue("@Skin", Skin.First().SkinId);
                RunCommand(AddSkins);
                AddSkins.Parameters.Clear();
            }
        }
        public void AddAccount(Account account, Character character)
        {
                AddAuthors.Parameters.AddWithValue("@User", account.Id);
            AddAuthors.Parameters.AddWithValue("@Character", character.CharacterId);
                RunCommand(AddSkins);
                AddSkins.Parameters.Clear();
        }
        public void AddMask(Mask mask, Character character)
        {
            var Mask = from masks in MaskService.GetMasks() where masks.MaskId == mask.MaskId select masks;
            AddMasks.Parameters.AddWithValue("@Character", character.CharacterId);
            if (Mask.Count() == 0)
            {
                MaskService.InsertMask(mask);
                var InsMask = from masks in MaskService.GetMasks() where masks.MaskType == mask.MaskType select masks;
                AddMasks.Parameters.AddWithValue("@Mask", InsMask.First().MaskId);
                RunCommand(AddMasks);
                AddMasks.Parameters.Clear();
            }
            else
            {
                AddMasks.Parameters.AddWithValue("@Mask", Mask.First().MaskId);
                RunCommand(AddMasks);
                AddMasks.Parameters.Clear();
            }
        }

        public Dictionary<Character, Account> GetCharactersWithAccounts()
        {
            var result = new Dictionary<Character, Account>();
            var BuferedBounds = GetBufferedData(SelectAuthors);
            foreach(var bound in BuferedBounds)
            {
                var character = from characters in GetCharacters() where characters.CharacterId == (int)bound[0] select characters;
                var account = from accounts in AccountService.GetAccountsCollection() where accounts.Id == (int)bound[1] select accounts;
                result.Add(character.First(), account.First());
            }
            return result;
        }

        public Dictionary<Character, Skin> GetCharactersWithSkins()
        {
            var result = new Dictionary<Character, Skin>();
            var BuferedBounds = GetBufferedData(SelectSkins);
            foreach (var bound in BuferedBounds)
            {
                var character = from characters in GetCharacters() where characters.CharacterId == (int)bound[0] select characters;
                var skin = from skins in SkinService.GetSkins() where skins.SkinId == (int)bound[1] select skins;
                result.Add(character.First(), skin.First());
            }
            return result;
        }

        public Dictionary<Character, Location> GetCharactersWithLocations()
        {
            var result = new Dictionary<Character, Location>();
            var BuferedBounds = GetBufferedData(SelectLocations);
            foreach (var bound in BuferedBounds)
            {
                var character = from characters in GetCharacters() where characters.CharacterId == (int)bound[0] select characters;
                var location = from locations in LocationService.GetLocations() where locations.LocationId == (int)bound[1] select locations;
                result.Add(character.First(), location.First());
            }
            return result;
        }

        public Dictionary<Character, Mask> GetCharactersWithMasks()
        {
            var result = new Dictionary<Character, Mask>();
            var BuferedBounds = GetBufferedData(SelectMasks);
            foreach (var bound in BuferedBounds)
            {
                var character = from characters in GetCharacters() where characters.CharacterId == (int)bound[0] select characters;
                var mask = from masks in MaskService.GetMasks() where masks.MaskId == (int)bound[1] select masks;
                result.Add(character.First(), mask.First());
            }
            return result;
        }

        public void InsertCharacter(Account account, Character character, Location location, Skin skin)
        {
            ArticleService.InsertArticle(character.CharacterArticle);
            var InsertedArticle = from articles in ArticleService.GetArticles() 
                                  where articles.ArticleText == character.CharacterArticle.ArticleText 
                                  select articles;
            InsertCharacters.Parameters.AddWithValue("@Article",InsertedArticle.First().ArticleId);
            InsertCharacters.Parameters.AddWithValue("@Name",character.CharacterName);
            InsertCharacters.Parameters.AddWithValue("@Gender", character.CharacterGender);
            InsertCharacters.Parameters.AddWithValue("@Age", character.CharacterAge);
            InsertCharacters.Parameters.AddWithValue("@Zodiak", character.CharacterZodiak);
            InsertCharacters.Parameters.AddWithValue("@Blood", character.CharacterBlood);
            InsertCharacters.Parameters.AddWithValue("@Birthday", character.CharacterBirthday);
            InsertCharacters.Parameters.AddWithValue("@Weight", character.CharacterWeight);
            InsertCharacters.Parameters.AddWithValue("@Ability", character.CharacterAbility);
            InsertCharacters.Parameters.AddWithValue("@Status", character.CharacterStatus);
            RunCommand(InsertCharacters);
            InsertCharacters.Parameters.Clear();
            var InsertedCharacter = from characters in GetCharacters() where characters.CharacterName == character.CharacterName select characters;
            AddAccount(account,InsertedCharacter.First());
            AddLocation(location, InsertedCharacter.First());
            AddSkin(skin, InsertedCharacter.First());
        }

    }
}
