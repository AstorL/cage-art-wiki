using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace Blazor_CageArt.Data
{
    public class ThreadService:MariaDBResolver<Thread>
    {
        private readonly IConfiguration Configuration;
        private AccountService AccountService;
        private MySqlCommand GetThreads= new MySqlCommand("SELECT * FROM threads");
        MySqlCommand InsertThread= new MySqlCommand(@"INSERT INTO threads
        (thread_name,thread_description,creation_date,thread_author_id) VALUES 
        (@Tittle, @Description, CURRENT_DATE(), @Author)");
        public ThreadService(IConfiguration Configuration) : base(Configuration)
        {
            this.Configuration = Configuration;
            AccountService = new AccountService(this.Configuration);
        }
        protected override ObservableCollection<Thread> GetData(MySqlCommand command)
        {
            var Result= new ObservableCollection<Thread>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach (var thread in BufferedData)
                {
                    var account = from accounts in AccountService.GetAccountsCollection() where accounts.Id==(int)thread[4] select accounts;
                    Result.Add(new Thread 
                    {
                        ThreadId=(int)thread[0],
                        ThreadName=(string)thread[1],
                        ThreadDescription=(string)thread[2],
                        CreationDate=(DateTime)thread[3],
                        ThreadAuthor= account.First()
                    });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"������ � �������� ������\n {E.Message}\n {E.StackTrace}");
            }
            return Result;
        }
        public ObservableCollection<Thread> GetAllThreads()=> GetData(GetThreads);
        public void AddThread(Thread thread)
        {
            InsertThread.Parameters.AddWithValue("@Tittle",thread.ThreadName);
            InsertThread.Parameters.AddWithValue("@Description",thread.ThreadDescription);
            InsertThread.Parameters.AddWithValue("@Author",thread.ThreadAuthor.Id);
            RunCommand(InsertThread);
            InsertThread.Parameters.Clear();
        }

    }
}