﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor_CageArt.Data
{
    public class GalleryService:MariaDBResolver<GalleryImage>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectImages = new MySqlCommand("SELECT * FROM gallery_index");
        public GalleryService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;
        protected override ObservableCollection<GalleryImage> GetData(MySqlCommand command)
        {
            var GalleryService = new ObservableCollection<GalleryImage>();
            var BufferedData = GetBufferedData(command);
            try
            {
                foreach(var image in BufferedData)
                {
                    GalleryService.Add(new GalleryImage { ImageId = (int)image[0], Path = (string)image[1] });
                }
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в получении галереи\n {E.Message}\n {E.StackTrace}\n");
            }
            return GalleryService;
        }

        public ObservableCollection<GalleryImage> GetGalleries() => GetData(SelectImages);
    }
}
