using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;

namespace Blazor_CageArt.Data
{

    public class NavigationService : MariaDBResolver<NavigationLink>
    {
        private IConfiguration Configuration;
        private MySqlCommand NavLinksRequest = new MySqlCommand("SELECT * FROM access_links");

        public NavigationService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;

        protected override ObservableCollection<NavigationLink> GetData(MySqlCommand comm)
        {
            var NavigationService = new ObservableCollection<NavigationLink>();
            var Buffer = GetBufferedData(comm);
            foreach (var link in Buffer)
            {
                try
                {
                    NavigationService.Add
                    (
                        new NavigationLink
                        {
                            IdLink=(int)link[0],
                            Adress=(string)link[1],
                            Header=(string)link[2],
                            Icon=(string)link[3],
                            AccessLevel=(string)link[4]
                        }
                    );
                }
                catch (Exception E)
                {
                    Console.WriteLine("Ошибка в получении навигационных ссылок \n" + E.Message + "\n" + E.StackTrace + "\n");
                }
            }
            Buffer.Clear();
            return NavigationService;
        }

        public ObservableCollection<NavigationLink> GetNavigationLinks() => GetData(NavLinksRequest);
    }
}