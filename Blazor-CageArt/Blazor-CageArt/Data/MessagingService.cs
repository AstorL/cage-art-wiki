using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace Blazor_CageArt.Data
{
    public class MessagingService : MariaDBResolver<Message>
    {
        private IConfiguration Configuration;
        private MySqlCommand GetMessages = new MySqlCommand("SELECT * FROM thread_messages");
        private MySqlCommand InsertMessage = new MySqlCommand(@"INSERT INTO thread_messages 
        (message,thread_id,user_id,message_date) VALUES(@Message,@Thread,@User,CURRENT_TIMESTAMP())");
        private ThreadService ThreadService;
        private AccountService AccountService;
        public MessagingService(IConfiguration Configuration) : base(Configuration)
        {
            this.Configuration = Configuration;
            ThreadService = new ThreadService(this.Configuration);
            AccountService = new AccountService(this.Configuration);
        }
        protected override ObservableCollection<Message> GetData(MySqlCommand command)
        {
            var Result = new ObservableCollection<Message>();
            var Messages = GetBufferedData(command);
            try
            {
                foreach (var message in Messages)
                {
                    var thread = from threads in ThreadService.GetAllThreads() where threads.ThreadId==(int)message[2] select threads;
                    var account = from accounts in AccountService.GetAccountsCollection() where accounts.Id == (int)message[3] select accounts;
                    Result.Add(new Message
                    {
                        MessageId = (int)message[0],
                        MessageText = (string)message[1],
                        MessageThread = thread.First(),
                        MessageAuthor= account.First(),
                        MessageDate= (DateTime)message[4]
                    });
                }
            }
            catch (Exception E)
            {
                Console.WriteLine($"������ � ��������� ������\n {E.Message}\n {E.StackTrace}\n");
            }
            return Result;
        }

        public ObservableCollection<Message> GetThreads() => GetData(GetMessages);
        public void SendMessage(Message message)
        {
            InsertMessage.Parameters.AddWithValue("@Message", message.MessageText);
            InsertMessage.Parameters.AddWithValue("@Thread", message.MessageThread.ThreadId);
            InsertMessage.Parameters.AddWithValue("@User", message.MessageAuthor.Id);
            RunCommand(InsertMessage);
            InsertMessage.Parameters.Clear();
        }
    }
}