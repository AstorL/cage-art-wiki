﻿using System;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.ObjectModel;

namespace Blazor_CageArt.Data
{
    public class AccountService : MariaDBResolver<Account>
    {
        private IConfiguration Configuration;
        private MySqlCommand SelectAccount = new MySqlCommand("SELECT * FROM account_access");
        private MySqlCommand InsertAccount = new MySqlCommand(@"INSERT INTO users (login,password,registration_date)
        VALUES (@Login,@Password,CURRENT_DATE())");
        public AccountService(IConfiguration Configuration) : base(Configuration) => this.Configuration = Configuration;

        protected override ObservableCollection<Account> GetData(MySqlCommand command)
        {
            var AccountService = new ObservableCollection<Account>();
            var BufferList = GetBufferedData(command);
            try
            {
                foreach (var account in BufferList)
                {
                    AccountService.Add(new Account
                    {
                        Id = (int)account[0],
                        Login = (string)account[1],
                        Password = (string)account[2],
                        RegistrationDate = (DateTime)account[3],
                        UserRole= (string)account[4]
                    });
                }
            }
            catch(Exception E)
            {
                Console.WriteLine($"Ошибка в получении учетных записей \n {E.Message} \n {E.StackTrace}");
            }
            BufferList.Clear();
            return AccountService;
        }
        public ObservableCollection<Account> GetAccountsCollection() =>GetData(SelectAccount);
        public void InsertAccounts(Account account)
        {
            InsertAccount.Parameters.AddWithValue("@Login", account.Login);
            InsertAccount.Parameters.AddWithValue("@Password", account.EnycryptPassword());
            RunCommand(InsertAccount);
            InsertAccount.Parameters.Clear();
        }
    }
}
